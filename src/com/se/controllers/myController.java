package com.se.controllers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.ModelMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//import com.se.services.Servers;

@Controller

public class myController {
	static String			url		= "jdbc:mysql://ec2jtlawson.ddns.net:3306/myDB";
	static String			user		= "jtlawson";
	static String			password		= "password";
	static Connection		connection	= null;
	String jsonInString = "{}"; 

	private final ObjectMapper mapper = new ObjectMapper();
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getString", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String printHello(ModelMap model) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = null;
			connection = DriverManager.getConnection(url, user, password);
			String selectSQL = "SELECT * FROM groupChat";
			PreparedStatement preparedStatement = connection.prepareStatement(selectSQL);
			ResultSet rs = preparedStatement.executeQuery();
			
			JSONArray obj = new JSONArray();
			while (rs.next()) {
				String id = "John"; //rs.getString("ID");
				String groomer = "Brook";//rs.getString("MESSAGE");
				String rdate = "test";//rs.getString("RDATE");
				String rtime = "test1";
				
				JSONObject list = new JSONObject();
				list.put("id", id);
				list.put("groomer", groomer);
				list.put("rdate", rdate);
				list.put("rtime", rtime);
				obj.add(list);	
			 }
				jsonInString = mapper.writeValueAsString(obj);  
		} catch (ClassNotFoundException | SQLException | JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonInString;

	}

	@RequestMapping(value = "/sendAppointment", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public void getMessage(@RequestBody String messageIn) throws IOException {
		
		String groomer = messageIn.split("\"")[4];
		String id = messageIn.split("\"")[7];
		String rdate = messageIn.split("\"")[10];
		String rtime = messageIn.split("\"")[13];

		
		System.out.println(groomer);
		System.out.println(id);
		System.out.println(rdate);
		System.out.println(rtime);
	/*	
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = null;
			connection = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PreparedStatement preparedStatement;
		try {
			String insertSQL = "INSERT INTO groupChat (ID, MESSAGE, RDATE) VALUES (?,?,?)";	
			preparedStatement = connection.prepareStatement(insertSQL);
			preparedStatement.setString(1, id);
			preparedStatement.setString(2, groomer);
			preparedStatement.setString(3, rdate);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
	}

}

