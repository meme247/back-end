<%-- 
    Document   : schedule
    Created on : Nov 20, 2017, 7:00:22 PM
    Author     : nateb
--%>

<%@page import="com.meme.dream.util.MLogger"%>
<%@page import="com.meme.dream.util.MemeSQL"%>
<%@page import="com.meme.dream.util.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Appointment</title>
    </head>
    <body>
        <h1>Let's Schedule an appointment! </h1>
        <%
            if(session.getAttribute("user") == null){
                response.sendRedirect("./login.jsp");
            }
            
            User u = (User) session.getAttribute("user"); 
            if(u == null || u.isValid() == false || u.getID() == -1){
                response.sendRedirect("./login.jsp");
            } else if(u.getType() != 1) {
                %>
                <h1> You're a groomer! You will be unable to schedule an appointment. This is for reference only! </h1> 
                <%
            }

            /* Alright, I assume everything here is now good... */ 
            %> 
            <form action="./..//schedule" method="POST">
                <select name="groomer">
                 <%
                    //Little hacky 
                    String[] groomers = MemeSQL.getGroomers().split("\\|");
                    MLogger.log("Got " + groomers.length + " groomers"); 
                    for(String s : groomers){
                     String[] data = s.split(","); 
                     MLogger.log("Working on groomer nugget: " + s); 
                     %>
                     <option value="<%=data[1]%>"><%=data[0]%></option> 
                     <%
                    }
                 %>
                </select>
                
                <input type="date" name="date">
                <input type="time" name="time"> 
                <input type="submit" value="Submit">
            </form>

  
    </body>
</html>
