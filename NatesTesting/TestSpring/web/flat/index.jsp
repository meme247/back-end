<%-- 
    Document   : index
    Created on : Nov 15, 2017, 7:25:01 PM
    Author     : nateb
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Testing</title>
        <style>
            h1,h2{
                text-align:center;
              }
            h1{
              color:rgba(100, 50, 255, .8);
            }
            .rainbow {
               /* Chrome, Safari, Opera */
              -webkit-animation: rainbow 1s infinite; 

              /* Internet Explorer */
              -ms-animation: rainbow 1s infinite;

              /* Standar Syntax */
              animation: rainbow 1s infinite; 
            }

            /* Chrome, Safari, Opera */
            @-webkit-keyframes rainbow{
                    20%{color: red;}
                    40%{color: yellow;}
                    60%{color: green;}
                    80%{color: blue;}
                    100%{color: orange;}	
            }
            /* Internet Explorer */
            @-ms-keyframes rainbow{
                    20%{color: red;}
                    40%{color: yellow;}
                    60%{color: green;}
                    80%{color: blue;}
                    100%{color: orange;}	
            }

            /* Standar Syntax */
            @keyframes rainbow{
                    20%{color: red;}
                    40%{color: yellow;}
                    60%{color: green;}
                    80%{color: blue;}
                    100%{color: orange;}	
            }

         </style>
    </head>
    <body>
        <h1 class="rainbow">Welcome to the testing page</h1>
        <h4> These pages are flat, not pretty, and used for testing the back end API </h4> 
        
        <list>
            <li><a href="./login.jsp">Test Login</a></li> 
            <li><a href="./register.jsp">Test Registration</a> </li> 
            <li><a href="./sessionInfo.jsp">Get Session Info</a> </li> 
            <li> <a href="./destroySession.jsp">Destroy Session</a> </li> 
            <li> <a href="./schedule.jsp">Make Appt</a> </li> 
            <li> <a href="./appointmentGet.jsp">List Appt</a> </li> 
            <li> <a href="./../getgroomers">Get Groomer List (JSON)</a> </li> 
            <li> <a href="./submitFeedback.jsp">Submit feedback</a> </li>
            <li> <a href="./feedbackGet.jsp">view your feedback</a> </li>
            <li> <a href="./uploadPicture.jsp">Upload Picture</a> </li>
            <li> <a href="./showImages.jsp">Show images</a> </li>
        <list> 
        
    </body>
</html>
