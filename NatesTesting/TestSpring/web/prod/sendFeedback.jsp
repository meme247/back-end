<%-- 
    Document   : index
    Created on : Nov 27, 2017, 1:59:39 PM
    Author     : nateb
--%>

<%@page import="com.meme.dream.util.MLogger"%>
<%@page import="com.meme.dream.util.MemeSQL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        	<style>
		body{
			background-color:#919191; 
		}
	</style>
    </head>
    <body>
        <%@include file="./navbar.jsp" %>
        <h1 style="color: #919191">Hello, world!</h1>
	<div class="container">
        <form class="form-horizontal" role="form" method="POST" action="./../feedback">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h2>Enter your Feedback below</h2>
                    <%
                        User u2 = (User) session.getAttribute("user"); 
                        boolean isGroomer = false; 
                        if(u2.getType() != 1){
                            isGroomer = true; 
                            %>
                            <h2> You're a groomer, this page is provided for reference only! </h2> 
                            <%
                        }
                    %> 
                    
                    
                    
                    <hr>
                </div>
            </div>
			
			<div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="sr-only" for="groomer">Groomer</label>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-male"></i></div>
                            <select name="groomer">
                            <%
                                //Little hacky 
                                String[] groomers = MemeSQL.getGroomers().split("\\|");
                                MLogger.log("Got " + groomers.length + " groomers"); 
                                for(String s : groomers){
                                 String[] data = s.split(","); 
                                 MLogger.log("Working on groomer nugget: " + s); 
                                 %>
                                 <option value="<%=data[1]%>"><%=data[0]%></option> 
                                 <%
                                }
                            %>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
			
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group has-danger">
                        <label class="sr-only" for="email">Feedback</label>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-pencil-square"></i></div>
                             <textarea class="form-control" rows="5" id="feedback" name="feedback" placeholder="Enter your feedback here"></textarea>
                        </div>
                    </div>
                </div>
            </div>
			
			
			
			
            <div class="row" style="padding-top: 1rem">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <% 
                        if(isGroomer){
                             
                        } else {
                    %>
                        <button type="submit" class="btn" style="color: #fff; background-color: #343a40!important; border-color: #343a40!important;"><i class="fa fa-sign-in"></i> Send Feedback</button>
                    <% } %> 
                
                </div>
            </div>
        </form>
    </div>
        
    </body>
</html>
