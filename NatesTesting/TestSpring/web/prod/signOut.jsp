<%-- 
    Document   : index
    Created on : Nov 27, 2017, 1:59:39 PM
    Author     : nateb
--%>

<%@page import="com.meme.dream.util.User"%>
<%@page import="com.meme.dream.util.MLogger"%>
<%@page import="com.meme.dream.util.MemeSQL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        	<style>
		body{
			background-color:#919191; 
		}
	</style>
    </head>
    <body>
        <%
            User u = (User) session.getAttribute("user"); 
            if(u != null){
                u.destroy(); 
            }
            
            session.invalidate(); 
            Cookie noMoreName = new Cookie("name", "");
            noMoreName.setMaxAge(0); 
            noMoreName.setPath("/"); 
            response.addCookie(noMoreName);
            
            Cookie noMoreType = new Cookie("type", "");
            noMoreType.setMaxAge(0); 
            noMoreType.setPath("/"); 
            response.addCookie(noMoreType);
  
         %> 
         
         <form action="./login.jsp"> 
         <div class="row" style="padding-top: 1rem">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button type="submit" class="btn" style="color: #fff; background-color: #343a40!important; border-color: #343a40!important;"><i class="fa fa-sign-in"></i> Back Home</button>
                </div>
            </div>
         </form> 
    </body> 
</html>
