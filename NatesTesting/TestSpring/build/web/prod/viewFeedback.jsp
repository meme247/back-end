<%-- 
    Document   : index
    Created on : Nov 27, 2017, 1:59:39 PM
    Author     : nateb
--%>

<%@page import="com.meme.dream.util.User"%>
<%@page import="com.meme.dream.util.MLogger"%>
<%@page import="com.meme.dream.util.MemeSQL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        	<style>
		body{
			background-color:#919191; 
		}
                        /* 
        Generic Styling, for Desktops/Laptops 
        */
        table { 
          width: 100%; 
          border-collapse: collapse; 
        }
        /* Zebra striping */
        tr:nth-of-type(odd) { 
          background: #eee; 
        }
        th { 
          background: #343a40!important; 
          color: white; 
          font-weight: bold; 
        }
        td, th { 
          padding: 6px; 
          border: 1px solid #ccc; 
          text-align: left; 
        }
        /* 
        Max width before this PARTICULAR table gets nasty
        This query will take effect for any screen smaller than 760px
        and also iPads specifically.
        */
        @media 
        only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px)  {

                /* Force table to not be like tables anymore */
                table, thead, tbody, th, td, tr { 
                        display: block; 
                }

                /* Hide table headers (but not display: none;, for accessibility) */
                thead tr { 
                        position: absolute;
                        top: -9999px;
                        left: -9999px;
                }

                tr { border: 1px solid #ccc; }

                td { 
                        /* Behave  like a "row" */
                        border: none;
                        border-bottom: 1px solid #eee; 
                        position: relative;
                        padding-left: 50%; 
                }

                td:before { 
                        /* Now like a table header */
                        position: absolute;
                        /* Top/left values mimic padding */
                        top: 6px;
                        left: 6px;
                        width: 45%; 
                        padding-right: 10px; 
                        white-space: nowrap;
                }


        }

	</style>
    </head>
    <body>
        <%@include file="./navbar.jsp" %>
        <h1 style="color: #919191">Hello, world!</h1>
        <table>
        <thead>
                <tr> 
                        <th> Name </th> 
                        <th> Feedback </th> 
                </tr> 
        </thead>
        <tbody>
        
        <%
            User u2 = (User)session.getAttribute("user"); 
            String s = ""; 
            if(u2 == null){
                s = "User was null?"; 
            }else if(u.getType() == 1){
                s = MemeSQL.getCustFeedbackHTML(u2.getID()); 
            } else {
                s = MemeSQL.getGroomerFeedbackHTML(u2.getID()); 
            }
        %> 
        <%= s %> 
        </tbody> 
	</table>	
        
        
        
    </body>
</html>
