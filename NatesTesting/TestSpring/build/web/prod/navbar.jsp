    <%@page import="com.meme.dream.util.User"%>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        
<nav class="navbar navbar-dark bg-dark">
  <ul class="nav">
  <!-- Home --> 
  <li class="nav-item">
    <a class="nav-link text-white" href="#">Groomer App</a>
  </li>
	<!-- Appointments Drop down --> 
	<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Appointments</a> 
		<div class="dropdown-menu">
			<a class="dropdown-item " href="/TestSpring/prod/schedule.jsp">Make an Appointment</a> 
			<a class="dropdown-item" href="/TestSpring/prod/calendar.jsp">View your Appointments</a> 
		</div> 
	</li> 
  
  <!-- Photo Drop down --> 
	<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Photos</a> 
		<div class="dropdown-menu">
			<a class="dropdown-item " href="/TestSpring/prod/viewPhotos.jsp">Gallery</a> 
			<a class="dropdown-item" href="/TestSpring/prod/upload.jsp">Upload a photo</a> 
		</div> 
	</li> 
        
	
	<!-- Feedback Drop down --> 
	<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Feedback</a> 
		<div class="dropdown-menu">
			<a class="dropdown-item" href="/TestSpring/prod/sendFeedback.jsp">Submit Feedback</a> 
			<a class="dropdown-item" href="/TestSpring/prod/viewFeedback.jsp">View your Feedback</a> 
		</div> 
	</li>
        
        <%
            User u = (User) session.getAttribute("user"); 
            if(u == null || u.isValid() == false){
                String url = request.getRequestURL().toString();
                if(url.contains(("login")) || url.contains("register") || url.contains("raw")){
                    
                } else {
                    %> 
                     <c:redirect url="./login.jsp"/>
                     <%
                }
            } else {
                %>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="/TestSpring/prod/signOut.jsp">Sign Out</a>
                        </li>
                 <!-- Right --> 
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link text-white" href="#">Hello, <%= u.getName() %></a>
                         </li>
                     </ul>
                 <%
            }
            
            %> 
        </ul>
     
</nav> 