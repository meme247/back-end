<%-- 
    Document   : index
    Created on : Nov 27, 2017, 1:59:39 PM
    Author     : nateb
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        	<style>
		body{
			background-color:white; 
		}
	</style>
    </head>
	<script src="./../../codebase/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="./../../codebase/dhtmlxscheduler.css" type="text/css" charset="utf-8">

	
<style type="text/css" >
	html, body{
		margin:0px;
		padding:0px;
		height:100%;
		overflow:hidden;
	}	
</style>

<script type="text/javascript" charset="utf-8">
function init() {
    
                scheduler.xy.scale_height = 40; //sets the height of the X-Axis  
		scheduler.config.xml_date="%Y-%m-%d %H:%i";
		scheduler.config.api_date="%Y-%m-%d %H:%i";
		scheduler.config.readonly = true;
		scheduler.init('scheduler_here',new Date(),"week");
                
                
                var json = ""; 
		var request = new XMLHttpRequest();
		request.open('GET', '/TestSpring/appointments', true);
		// this following line is needed to tell the server this is a ajax request
		request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		request.onload = function () {
			if (this.status >= 200 && this.status < 400) {
				json = JSON.parse(this.response);
                                	
                                for(var appt in json){
                                    var date = json[appt]["date"] + ""; 
                                    var groomer = json[appt]["name"]; 
                                                        var date2 = advance(date); 


                                    console.log("Date: " + date); 
                                    console.log("Adv : " + advance(date)); 

                                    scheduler.addEvent({
                                            start_date: date,
                                            end_date:   date2,
                                            text:   groomer
                                    });	
                                }     
			}
		};
		
		request.send();	
	}
	
	function advance(d){
		var data = d.split(" "); 
		var left = data[0]; 		
		
		// Proc time 
		var d2 = data[1].split(":");
		var hour = parseInt(d2[0])+1; 
	
		var finishUp = left+" "+hour+":"+d2[1]+":00";
		return finishUp; 
	}
</script>

<body onload="init();">
        <!-- Cant get navbar to load --> 
	<div id="scheduler_here" style='width:100%; height:100%;'>
	<div class="dhx_cal_navline">
			<div class="dhx_cal_prev_button">&nbsp;</div>
			<div class="dhx_cal_next_button">&nbsp;</div>
			<div class="dhx_cal_today_button"></div>
			<div class="dhx_cal_date"></div>
			<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
			<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
			<div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
		</div>
		<div class="dhx_cal_header">
		</div>
		<div class="dhx_cal_data">
		</div>
	</div> 
</body>
</html>
