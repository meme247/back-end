<%-- 
    Document   : index
    Created on : Nov 27, 2017, 1:59:39 PM
    Author     : nateb
--%>

<%@page import="com.meme.dream.util.MLogger"%>
<%@page import="com.meme.dream.util.MemeSQL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        	<style>
		body{
			background-color:#919191; 
		}
	</style>
    </head>
    <body>
        <%@include file="./navbar.jsp" %>
          <h1 style="color: #919191">Hello, world!</h1>
	<div class="container">
        <form class="form-horizontal" role="form" method="POST" action="./../schedule">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h2>Schedule Appointment</h2>
                    <hr>
                </div>
            </div>
			
			<div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">					
						<label for="groomer"><b>Groomer</b></label> 
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-male"></i></div>
                         <select name="groomer">
                            <%
                                //Little hacky 
                                String[] groomers = MemeSQL.getGroomers().split("\\|");
                                MLogger.log("Got " + groomers.length + " groomers"); 
                                for(String s : groomers){
                                 String[] data = s.split(","); 
                                 MLogger.log("Working on groomer nugget: " + s); 
                                 %>
                                 <option value="<%=data[1]%>"><%=data[0]%></option> 
                                 <%
                                }
                            %>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
			
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group has-danger">
						<label><b>Date</b></label>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-calendar"></i></div>
                            <input type="date" name="date" > 
                        </div>
                    </div>
                </div>
            </div>
			
			<div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group has-danger">
						<label><b>Time</b></label>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-clock-o"></i></div>
								<select name="time">
									<option value="10:00">10 AM</option>
									<option value="11:00">11 AM</option>
									<option value="12:00">12 PM</option>
									<option value="13:00">1 PM</option>
									<option value="14:00">2 PM</option>
									<option value="15:00">3 PM</option>
									<option value="16:00">4 PM</option>
								</select> 
							
                        </div>
                    </div>
                </div>
            </div>
			
			
			
			
            <div class="row" style="padding-top: 1rem">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button type="submit" class="btn" style="color: #fff; background-color: #343a40!important; border-color: #343a40!important;"><i class="fa fa-sign-in"></i> Schedule</button>
                </div>
            </div>
        </form>
    </div>
        
    </body>
</html>
