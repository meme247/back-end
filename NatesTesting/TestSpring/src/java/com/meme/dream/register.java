package com.meme.dream;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.meme.dream.util.MLogger; 
import com.meme.dream.util.MemeSQL;
import com.meme.dream.util.User;

@Controller
@RequestMapping("/register")
public class register {
    
   @RequestMapping(method = RequestMethod.POST)
   public String requestHandlingMethod(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
      if(request.getParameter("email") != null && request.getParameter("password") != null){
          
      } else {
          MLogger.log("Seems we're null in the registration"); 
          model.addAttribute("message", "Error :( Email or Pass was null");
          model.addAttribute("abort", "<button class=\"btn\" onclick=\"goBack()\">Go Back</button><script> function goBack() {window.history.back();}</script>"); 
          return "raw"; 
      }
      
      String email = request.getParameter("email").toLowerCase();
      
      
      if(MemeSQL.isEmailTaken(email) == true){
          MLogger.log("Email already taken"); 
          model.addAttribute("message", "That email is already taken!"); 
          model.addAttribute("abort", "<button class=\"btn\" onclick=\"goBack()\">Go Back</button><script> function goBack() {window.history.back();}</script>"); 
          return "raw"; 
      }
       
      MLogger.log("Attempting to register... " + email); 
      
      String pass = MemeSQL.MD5(request.getParameter("password")); 

      String user = request.getParameter("user"); 
      int type = Integer.parseInt(request.getParameter("type")); 
      
      MLogger.log("Email: " + email + " Pass: " + pass + " user: " + user + " type: " + type); 
      MemeSQL.addUser(email, pass, user, type); 
      MLogger.log("User added... Maybe"); 
      
      model.addAttribute("message", "User created!"); 
      model.addAttribute("abort", "<input type=\"button\" onclick=\"window.open('/TestSpring/prod/login.jsp', '_self')\" value=\"Schedule Appointment\" />" ); 
      return "raw";
   }
}