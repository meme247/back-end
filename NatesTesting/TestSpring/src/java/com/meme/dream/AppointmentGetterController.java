package com.meme.dream;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.meme.dream.util.MLogger; 
import com.meme.dream.util.MemeSQL;
import com.meme.dream.util.User;

@Controller
@RequestMapping("/appointments")
public class AppointmentGetterController {
    
   @RequestMapping(method = {RequestMethod.POST,RequestMethod.GET})
   public String requestHandlingMethod(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
      response.addHeader("Access-Control-Allow-Origin", "*"); 
       User u = (User) request.getSession().getAttribute("user");
      if(u == null){
          //Not logged in. Throw them to the login. 
          model.addAttribute("message", "You must be logged in to perform this action!"); 
          return "raw"; 
      } 
      
      int type = u.getType(); 
      int id = u.getID(); 
      
      if(type == 1){ /* Customer */ 
          model.addAttribute("message", MemeSQL.getCustAppointments(id)); 
          return "json"; 
      } else {
          model.addAttribute("message", MemeSQL.getGroomerAppointments(id)); 
          return "json";
      }
   }
}