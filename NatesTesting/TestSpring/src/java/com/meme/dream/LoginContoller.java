package com.meme.dream;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.meme.dream.util.MLogger; 
import com.meme.dream.util.MemeSQL;
import com.meme.dream.util.User;
import javax.servlet.http.Cookie;

@Controller
@RequestMapping("/login")
public class LoginContoller {
    
   @RequestMapping(method = RequestMethod.POST)
   public String requestHandlingMethod(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
      if(request.getParameter("email") != null && request.getParameter("password") != null){
          
      } else {
          MLogger.log("Seems we're null in the login"); 
          model.addAttribute("message", "Error :( Email or Pass was null");
          model.addAttribute("abort", "<button class=\"btn\" onclick=\"goBack()\">Go Back</button><script> function goBack() {window.history.back();}</script>"); 
          return "error"; 
      }
       
      
       
      String email = request.getParameter("email").toLowerCase();
      MLogger.log("Attempting to log in " + email); 
      
      String pass = MemeSQL.MD5(request.getParameter("password")); 
      
      User u = MemeSQL.verifyUserLogin(email, pass, false);
      MLogger.log("MemeSQL returned " + u.isValid() + " for the login"); 
      
      if(u.isValid()){
        model.addAttribute("message", "Hello, " + u.getName()); 
        request.getSession().setAttribute("user", u);
        
        Cookie name = new Cookie("name", u.getName()); 
        Cookie type = new Cookie("type", u.getType()+"");
        
        response.addCookie(name);
        response.addCookie(type); 
        model.addAttribute("abort", "<input type=\"button\" onclick=\"window.open('/TestSpring/prod/schedule.jsp', '_self')\" value=\"Schedule Appointment\" />" ); 
        
      } else {
          model.addAttribute("message", "Sorry, but that's an invalid login!");
          model.addAttribute("abort", "<button class=\"btn\" onclick=\"goBack()\">Go Back</button><script> function goBack() {window.history.back();}</script>"); 
      }
      
      
      return "raw";
   }
}