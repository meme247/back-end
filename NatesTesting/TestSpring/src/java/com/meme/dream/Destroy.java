package com.meme.dream;

import com.meme.dream.util.MemeSQL;
import com.meme.dream.util.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/destroy")
public class Destroy {
    
   @RequestMapping(method = {RequestMethod.POST,RequestMethod.GET})
    public void requestHandlingMethod(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter(); 
        response.setContentType("text/plain");
        
        out.write("Falsifying User object...");
        User u = (User) request.getSession().getAttribute("user");
        
        if(u != null){
            u.destroy();
        }
        
        
        out.write("Invalidating session..."); 
        request.getSession().invalidate();
        
        out.write("Nom noming cookies"); 
        Cookie noMoreName = new Cookie("name", "");
        noMoreName.setMaxAge(0); 
        noMoreName.setPath("/"); 
        response.addCookie(noMoreName); 
        
        Cookie noMoreType = new Cookie("type", "");
        noMoreType.setMaxAge(0); 
        noMoreType.setPath("/"); 
        response.addCookie(noMoreType);
       
        out.write("Done..."); 
        
        out.close(); 
    }
}