package com.meme.dream;

import com.meme.dream.util.MemeSQL;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/echo")
public class Echo {
    
   @RequestMapping(method = {RequestMethod.POST,RequestMethod.GET})
    public void requestHandlingMethod(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter(); 
        response.setContentType("text/plain"); 
        
        Enumeration<String> parms = request.getParameterNames(); 
        
        while(parms.hasMoreElements()){
            String parmName = parms.nextElement(); 
            out.write(parmName); 
            out.write("\n"); 
            
            String[] vals = request.getParameterValues(parmName); 
            for(int i = 0; i < vals.length; i++){
                String parmValue = vals[i]; 
                out.write("\t" + parmValue); 
                out.write("\n"); 
            }
            
        }
        
        
        out.write("===== SESSION =====\n ");
        Enumeration<String> sparms = request.getSession().getAttributeNames();
        while(sparms.hasMoreElements()){
            String parmName = sparms.nextElement(); 
            out.write(parmName); 
            out.write("\n"); 
            
            Object o = request.getSession().getAttribute(parmName); 
            out.write("" + o); 
            
        }
        
        out.write("====== Cookie ==== \n");
        Cookie cookie = null;
         Cookie[] cookies = null;
         
         // Get an array of Cookies associated with the this domain
         cookies = request.getCookies();
         
         out.print("Cookie: |NAME_HERE|VALUE_HERE|\n");
         if( cookies != null ) {
            for (int i = 0; i < cookies.length; i++) {
               cookie = cookies[i];
               out.print("Cookie: |"+cookie.getName( )+"|"+cookie.getValue( )+"|\n");
            }
         } 
       
        out.close(); 
    }
}