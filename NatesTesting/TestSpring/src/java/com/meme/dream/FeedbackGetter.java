package com.meme.dream;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.meme.dream.util.MLogger; 
import com.meme.dream.util.MemeSQL;
import com.meme.dream.util.User;

@Controller
@RequestMapping("/getfeedback")
public class FeedbackGetter {
    
   @RequestMapping(method = {RequestMethod.POST,RequestMethod.GET})
   public String requestHandlingMethod(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
      User u = (User) request.getSession().getAttribute("user");
      if(u == null){
          //Not logged in. Throw them to the login. 
          model.addAttribute("message", "You must be logged in to perform this action!"); 
          return "raw"; 
      } 
      
      int type = u.getType(); 
      int id = u.getID(); 
      
      if(type == 1){ /* Customer */ 
          model.addAttribute("message", MemeSQL.getCustFeedback(id)); 
          return "raw"; 
      } else {
          model.addAttribute("message", MemeSQL.getGroomerFeedback(id)); 
          return "raw";
      }
   }
}