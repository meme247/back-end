package com.meme.dream;

import com.meme.dream.util.MemeSQL;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/getgroomers")
public class getGroomers {
    
   @RequestMapping(method = {RequestMethod.POST,RequestMethod.GET})
    public void requestHandlingMethod(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter(); 
        response.setContentType("application/json"); 
        out.print(MemeSQL.getGroomersJSON());
        out.close(); 
    }
}