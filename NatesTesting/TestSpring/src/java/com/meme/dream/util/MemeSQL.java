/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.meme.dream.util;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;


/*
Email is the main way to loging.
User is basically useless. But oh well.
*/

public class MemeSQL {
    private static final String dbClassName = "com.mysql.jdbc.Driver";
    private static final String CONNECTION = "jdbc:mysql://127.0.0.1/petGroomingDB";
    
    private static Connection getConnection(){
        try {
                Class.forName(dbClassName);
        } catch (ClassNotFoundException e) {
                e.printStackTrace();
        }


        Properties p = new Properties();
        p.put("user","root");
        p.put("password","google");

        Connection c;
        try {
                c = DriverManager.getConnection(CONNECTION,p);
                return c; 
        } catch (SQLException e) {
                e.printStackTrace();
        }
        return null; 
    }
    
    /*
    Type:
        0 - Customer
        1 - Groomer
    Return:
        0 - Failed, I think we'll get an error if we dupe email, so this could never come up in theory. 
        1 - Success
       -1 - Error
    */
    public static int addUser(String email, String pass, String user, int type){
        String sql = "INSERT INTO Users VALUES(?,?,?,?,?)"; 
        PreparedStatement stmt = null; 
        pass = MD5(pass);
        
        try{
            Connection conn = getConnection(); 
            stmt = conn.prepareStatement(sql); 
            
            stmt.setString(1, user); 
            stmt.setString(2, pass);
            stmt.setString(3, email);
            stmt.setInt(4, type); 
            stmt.setInt(5, 0); //This is useless, but it gets mad without it. 
            
            stmt.executeUpdate(); 
            
            stmt.close(); 
            conn.close();

            // Release the schmoo (hack)
            MLogger.log("About to modify the things"); 
            User u = verifyUserLogin(email, pass, true); 
            MLogger.log("Got a user: " + u); 
            int id = u.getID(); 
            
            /* Update the respective thingy */ 
            if(type == 1){ /*Customer*/ 
                insertCustomer(user, id); 
            } else { /* Groomer */ 
                insertGroomer(user, id); 
            }

            return 1; 
            
        }catch(Exception e){
            e.printStackTrace(); 
            return -1; 
        }
    }
       
    
    /* Could these be one method? I'm sure they could, but meh. */ 
    public static void insertGroomer(String user, int id){
        String sql = "INSERT INTO Groomers VALUES(?,?)"; 
        PreparedStatement stmt = null;
        
        try{
            Connection conn = getConnection(); 
            stmt = conn.prepareStatement(sql); 
            
            stmt.setString(1, user); 
            stmt.setInt(2, id); 

            stmt.executeUpdate(); 
            
            stmt.close(); 
            conn.close();
        }catch(Exception e){
            e.printStackTrace(); 
            MLogger.log("No bueno on adding a groomer there mate"); 
        }
    }
    
    public static void insertCustomer(String user, int id){
        String sql = "INSERT INTO Customers VALUES(?,?)"; 
        PreparedStatement stmt = null;
        
        try{
            Connection conn = getConnection(); 
            stmt = conn.prepareStatement(sql); 
            
            stmt.setString(1, user); 
            stmt.setInt(2, id); 

            stmt.executeUpdate(); 
            
            stmt.close(); 
            conn.close();
        }catch(Exception e){
            e.printStackTrace(); 
            MLogger.log("No bueno on adding a customer there mate"); 
        }
    }
    
    public static boolean isEmailTaken(String email){
        String sql = "SELECT * FROM Users WHERE email=?"; 
        PreparedStatement stmt = null; 
        
        try{
            Connection conn = getConnection(); 
            stmt = conn.prepareStatement(sql); 
            stmt.setString(1, email); 
            
            //Execute <gun_emoji> 
            ResultSet rs = stmt.executeQuery(); 
            
            //It was there!
            if(rs.next()){
                stmt.close(); 
                conn.close();
                return true;  
            } else {
                stmt.close(); 
                conn.close();
                return false;  
            }
        } catch(Exception e){
            e.printStackTrace(); 
            MLogger.log("Got an error in MemeSQL..."); 
            return false;  
        } 
    }
    
    
    /*
         0  - Failed
         1  - Success
        -1  - Error 
    */
    public static int verifyLogin(String email, String pass){
        String sql = "SELECT * FROM Users WHERE email=? and password=?"; 
        PreparedStatement stmt = null; 
        pass = MD5(pass); 
        
        try{
            Connection conn = getConnection(); 
            stmt = conn.prepareStatement(sql); 
            stmt.setString(1, email); 
            stmt.setString(2, pass); 
            
            //Execute <gun_emoji> 
            ResultSet rs = stmt.executeQuery(); 
            
            //It was there!
            if(rs.next()){
                stmt.close(); 
                conn.close();
                return 1;  
            } else {
                stmt.close(); 
                conn.close();
                return 0;  
            }
        } catch(Exception e){
            e.printStackTrace(); 
            MLogger.log("Got an error in MemeSQL..."); 
            return -1;  
        } 
    }
    
    public static User verifyUserLogin(String email, String pass, boolean hashed){
        String sql = "SELECT * FROM Users WHERE email=? and password=?"; 
        PreparedStatement stmt = null; 
        if(!hashed){
            pass = MD5(pass);
        }
        
        try{
            Connection conn = getConnection(); 
            stmt = conn.prepareStatement(sql); 
            stmt.setString(1, email); 
            stmt.setString(2, pass); 
            
            //Execute <gun_emoji> 
            ResultSet rs = stmt.executeQuery(); 
            
            //It was there!
            if(rs.next()){
                User u = new User(rs.getString("email"), rs.getString("username"), rs.getInt("type"), true, rs.getInt("id")); 
                
                stmt.close(); 
                conn.close();
                return u;  
            } else {
                stmt.close(); 
                conn.close(); 
            }
        } catch(Exception e){
            e.printStackTrace(); 
            MLogger.log("Got an error in MemeSQL..."); 
            return new User("ErrorInExecution", "", 0, false, -1);   
        } 
        
        return new User("Suppose there wasn't a user?", "", 0, false, -1); 
    }
  
   /*
    MD5 implementation. Secure enough for our uses.
    Even if this is a pure MD5 implementation it will still work as it's 
    hashed the same way each time, and that's what we need. 
    */
   public static String MD5(String md5) {
        try {
             java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
             byte[] array = md.digest(md5.getBytes());
             StringBuffer sb = new StringBuffer();
             for (int i = 0; i < array.length; ++i) {
               sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
             }
             return sb.toString();
         } catch (java.security.NoSuchAlgorithmException e) {
         }
         
            return null;
}
   
   public static String getGroomerAppointments(int id){
       String sql = "SELECT * FROM Appointments RIGHT JOIN Customers ON Appointments.custNum=Customers.custNum WHERE Appointments.employeeNum=?"; 
       
       try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           stmt.setInt(1, id); 
           
           //Gogo
           ResultSet rs = stmt.executeQuery(); 
           
           // Let's build us some JSON 
           JsonArrayBuilder jsonarray = Json.createArrayBuilder(); 
           while(rs.next()){
               jsonarray.add(Json.createObjectBuilder()
                    .add("date", rs.getTimestamp("apptDate").toString())
                    .add("name", rs.getString("firstName")
                    ).build()); 
           }
           
          stmt.close(); 
          conn.close();
          return jsonarray.build() + ""; 
           
           
       }catch(Exception e){
           e.printStackTrace(); 
           MLogger.log("Error getting groomer appointments"); 
       }
       
       return ""; 
       
   }
   
    public static String getCustAppointments(int id){
       String sql = "SELECT * FROM Appointments RIGHT JOIN Groomers ON Appointments.employeeNum=Groomers.employeeNum WHERE Appointments.custNum=?"; 
       
       try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           stmt.setInt(1, id); 
           
           //Gogo
           ResultSet rs = stmt.executeQuery(); 
           
           // Let's build us some JSON 
           JsonArrayBuilder jsonarray = Json.createArrayBuilder(); 
           while(rs.next()){
               jsonarray.add(Json.createObjectBuilder()
                    .add("date", rs.getTimestamp("apptDate").toString())
                    .add("name", rs.getString("firstName")
                    ).build()); 
           }
           
           
            stmt.close(); 
            conn.close();
           return jsonarray.build() + ""; 
           
           
       }catch(Exception e){
           e.printStackTrace(); 
           MLogger.log("Error getting customer appointments"); 
       }
       
       return ""; 
       
    }  
    
    public static String getGroomers(){
        String sql = "SELECT * FROM Groomers"; 
        
         try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           ResultSet rs = stmt.executeQuery(); 
           
           String groomers = ""; 
           while(rs.next()){
               groomers += rs.getString("firstName") + "," + rs.getInt("employeeNum") + "|"; 
           }

           MLogger.log("Got these groomers: " + groomers); 
            stmt.close(); 
            conn.close();
           return groomers; 
           
         }catch(Exception e){
             e.printStackTrace(); 
             MLogger.log("Couldn't get groomers!"); 
         }
        
        return "ERROR,ERROR";
    }
   
     public static String getGroomersJSON(){
        String sql = "SELECT * FROM Groomers"; 
        
         try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           ResultSet rs = stmt.executeQuery(); 
           
           JsonArrayBuilder jsonarray = Json.createArrayBuilder(); 
           while(rs.next()){
               jsonarray.add(Json.createObjectBuilder()
                    .add("id", rs.getInt("employeeNum"))
                    .add("name", rs.getString("firstName")
                    ).build()); 
           }

           String groomers = jsonarray.build() + ""; 
           MLogger.log("Got these groomers: " + groomers); 
            stmt.close(); 
            conn.close();
           return groomers;  
           
         }catch(Exception e){
             e.printStackTrace(); 
             MLogger.log("Couldn't get groomers!"); 
         }
        
        return "ERROR,ERROR";
    }

    public static void addAppointment(int id, String groomerID, Date date) {
       if(date == null){
           return; 
       }
       
       String sql = "INSERT INTO Appointments VALUES(?,?,?,?)";
       
       try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
                     
           
           //Date, Employee Num, Groomer Num, appt NUm
           stmt.setTimestamp(1, new java.sql.Timestamp(date.getTime()));
           stmt.setInt(2, Integer.parseInt(groomerID)); 
           stmt.setInt(3, id);
           stmt.setInt(4, 0); //AutoInc 
           MLogger.log("Query?: " + stmt.toString()); 
           stmt.executeUpdate(); //'tis an insert 
           
            stmt.close(); 
            conn.close();
           
       }catch(Exception e){
           e.printStackTrace(); 
           MLogger.log("Couldn't insert appointment..."); 
       }
       
       
       
    }

    public static void addFeedback(int id, String groomerID, String feedback) {
       String sql = "INSERT INTO feedback VALUE(?,?,?)"; 
       
       if(feedback.length() > 200){
           feedback = feedback.substring(0, 200); 
       }
       
       try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           stmt.setInt(1, Integer.parseInt(groomerID));
           stmt.setInt(2, id); 
           stmt.setString(3, feedback); 
           

           stmt.executeUpdate(); //'tis an insert 
           
            stmt.close(); 
            conn.close();
       }catch(Exception e){
           e.printStackTrace(); 
           MLogger.log("Couldn't insert appointment..."); 
       }
    }

    
    public static String getGroomerFeedback(int id){
       String sql = "SELECT * FROM feedback RIGHT JOIN Customers ON feedback.custNum=Customers.custNum WHERE feedback.employeeNum=?"; 
       
       try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           stmt.setInt(1, id); 
           
           //Gogo
           ResultSet rs = stmt.executeQuery(); 
           
           // Let's build us some JSON 
           JsonArrayBuilder jsonarray = Json.createArrayBuilder(); 
           while(rs.next()){
               jsonarray.add(Json.createObjectBuilder()
                    .add("custName", rs.getString("firstName"))
                    .add("feedback", rs.getString("feedback")
                    ).build()); 
           }
           
            stmt.close(); 
            conn.close();
           return jsonarray.build() + ""; 
           
           
       }catch(Exception e){
           e.printStackTrace(); 
           MLogger.log("Error getting groomer feedback"); 
       }
       
       return ""; 
    }
    
    public static String getGroomerFeedbackHTML(int id){
       String sql = "SELECT * FROM feedback RIGHT JOIN Customers ON feedback.custNum=Customers.custNum WHERE feedback.employeeNum=?"; 
       
       try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           stmt.setInt(1, id); 
           
           //Gogo
           ResultSet rs = stmt.executeQuery(); 
           
           // Let's build us some JSON 
           String html = "";  
           while(rs.next()){
                html+="<tr><td>"+rs.getString("firstName")+"</td><td>"+rs.getString("feedback")+"</td></tr>"; 
           }
           
            stmt.close(); 
            conn.close();
           return html; 
           
           
       }catch(Exception e){
           e.printStackTrace(); 
           MLogger.log("Error getting groomer feedback HTML"); 
       }
       
       return ""; 
    }
    
    public static String getCustFeedback(int id){
       String sql = "SELECT * FROM feedback RIGHT JOIN Groomers ON feedback.employeeNum=Groomers.employeeNum WHERE feedback.custNum=?"; 
       
       try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           stmt.setInt(1, id); 
           
           //Gogo
           ResultSet rs = stmt.executeQuery(); 
           
           // Let's build us some JSON 
           JsonArrayBuilder jsonarray = Json.createArrayBuilder(); 
           while(rs.next()){
               jsonarray.add(Json.createObjectBuilder()
                    .add("groomerName", rs.getString("firstName"))
                    .add("feedback", rs.getString("feedback")
                    ).build()); 
           }
           stmt.close(); 
            conn.close();
           return jsonarray.build() + ""; 
           
           
       }catch(Exception e){
           e.printStackTrace(); 
           MLogger.log("Error getting groomer feedback"); 
       }
       
       return ""; 
    }
     public static String getCustFeedbackHTML(int id){
       String sql = "SELECT * FROM feedback RIGHT JOIN Groomers ON feedback.employeeNum=Groomers.employeeNum WHERE feedback.custNum=?"; 
       
       try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           stmt.setInt(1, id); 
           
           //Gogo
           ResultSet rs = stmt.executeQuery(); 
           
           String html = ""; 
           while(rs.next()){
               html+="<tr><td>"+rs.getString("firstname")+"</td><td>"+rs.getString("feedback")+"</td></tr>"; 
           }
           
           stmt.close(); 
           conn.close();
           return  html; 
           
           
       }catch(Exception e){
           e.printStackTrace(); 
           MLogger.log("Error getting cust feedback HTML"); 
       }
       
       return ""; 
    }

    public static void addPhoto(int id, InputStream in) {
        String sql = "INSERT INTO photos VALUE(?,?)"; 

       try{
           Connection conn = getConnection(); 
           PreparedStatement stmt = conn.prepareStatement(sql); 
           
           stmt.setInt(1, id);
           stmt.setBlob(2, in);
           

           stmt.executeUpdate(); //'tis an insert 
           stmt.close(); 
           conn.close();
       }catch(Exception e){
           e.printStackTrace(); 
           MLogger.log("Couldn't insert appointment..."); 
       }
    }

    
}
