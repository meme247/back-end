/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.meme.dream.util;

/**
 *
 * @author nateb
 */
public class User {
    String email;
    String name;
    int type; 
    boolean isValid; 
    int id; 
    
    
    public User(String e, String n, int t, boolean i, int id){
        this.email = e;
        this.name = n; 
        this.type = t; 
        this.isValid = i; 
        this.id = id; 
    }
    
    public boolean isValid(){
        return this.isValid; 
    }
    
    public String getName(){
        return this.name; 
    }
    
    public int getType(){
        return this.type; 
    }
    
    public void destroy(){
        this.isValid = false; 
    }
    
    public int getID(){
        return this.id; 
    }
    
    @Override
    public String toString(){
        return "Email : " + email + " Username: " + name + " type: " + type + " isValid: " + isValid + " id: " + id; 
    }
    
}
