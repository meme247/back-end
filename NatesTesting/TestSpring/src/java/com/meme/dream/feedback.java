package com.meme.dream;

import com.meme.dream.util.MemeSQL;
import com.meme.dream.util.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/feedback")
public class feedback {
    
   @RequestMapping(method = {RequestMethod.POST,RequestMethod.GET})
    public String requestHandlingMethod(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        User u = (User) request.getSession().getAttribute("user"); 
        if(u == null){
            model.addAttribute("message", "User was null!"); 
            model.addAttribute("abort", "<button class=\"btn\" onclick=\"goBack()\">Go Back</button><script> function goBack() {window.history.back();}</script>");
            return "raw";        
        }
        
        String feedback = request.getParameter("feedback"); 
        String groomerID = request.getParameter("groomer"); 
        int id = u.getID(); 
        
        if(feedback != null && groomerID != null && id != -1){
            MemeSQL.addFeedback(id, groomerID, feedback);
            model.addAttribute("message", "Feedback sent!");
            model.addAttribute("abort", "<input type=\"button\" onclick=\"window.open('/TestSpring/prod/viewFeedback.jsp', '_self')\" value=\"View your Feedback\" />" );
            return "raw"; 
        } else {
            model.addAttribute("message", "Something was null");
            model.addAttribute("abort", "<button class=\"btn\" onclick=\"goBack()\">Go Back</button><script> function goBack() {window.history.back();}</script>"); 
            return "raw"; 
        }
    }
}