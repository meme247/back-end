package com.meme.dream;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.meme.dream.util.MLogger; 
import com.meme.dream.util.MemeSQL;
import com.meme.dream.util.User;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date; 

@Controller
@RequestMapping("/schedule")
public class AppointmentScheduler {
    
   @RequestMapping(method = RequestMethod.POST)
   public String requestHandlingMethod(ModelMap model, HttpServletRequest request, HttpServletResponse response){
      User u = (User) request.getSession().getAttribute("user");
      String groomerID = request.getParameter("groomer"); 
      String date = request.getParameter("date"); 
      String time = request.getParameter("time"); 
      
      if(groomerID == null || date == null || time == null ){
          //No bueno. 
          model.addAttribute("message", "Something was null"); 
          model.addAttribute("abort", "<button class=\"btn\" onclick=\"goBack()\">Go Back</button><script> function goBack() {window.history.back();}</script>"); 
          return "raw"; 
      }
          
      MLogger.log("ID: " + groomerID + " Date: " + date + " Time: " + time); 
      
      DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
      Date appt;
      try{
        appt = df.parse(date + " " + time);
        MLogger.log("From browser was okat"); 
      }catch(Exception e){
        e.printStackTrace(); 
        MLogger.log("Seems we had an invalid date, this is what I got |"+date+"| and |"+time+"|"); 
        model.addAttribute("message", "Invalid date format!"); 
        model.addAttribute("abort", "<button class=\"btn\" onclick=\"goBack()\">Go Back</button><script> function goBack() {window.history.back();}</script>"); 
        
        return "raw"; 
      }
      
      MLogger.log("Date: " + appt);
      MemeSQL.addAppointment(u.getID(), groomerID, appt); 
      
      model.addAttribute("message", "Appointment added!");
       model.addAttribute("abort", "<input type=\"button\" onclick=\"window.open('/TestSpring/prod/calendar.jsp', '_self')\" value=\"View your Appointments\" />" );
      return "raw"; 
     
   }
}