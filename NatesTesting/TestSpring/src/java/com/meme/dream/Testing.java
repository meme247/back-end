package com.meme.dream;

import com.meme.dream.util.MemeSQL;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/dosomething")
public class Testing {
    
   @RequestMapping(method = RequestMethod.GET)
    public void requestHandlingMethod(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("Inside of dosomething handler method");
        
        response.getWriter().println("HELLO! PLEASE SEE ME OH DEAR GOD I'M TRAPPED IN A JAVA SERVLET");

    }
}