package com.meme.dream;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.meme.dream.util.MLogger; 
import com.meme.dream.util.MemeSQL;
import com.meme.dream.util.User;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date; 
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;

@Controller
@RequestMapping("/uploadPhoto")
@MultipartConfig(maxFileSize = 16177215) 
public class uploadPhoto {
    
   @RequestMapping(method = RequestMethod.POST)
   public String requestHandlingMethod(ModelMap model, HttpServletRequest request, HttpServletResponse response){
      User u = (User) request.getSession().getAttribute("user");
      if(u == null){
          model.addAttribute("message", "User was null!"); 
          return "raw"; 
      }
      
      try{
        InputStream in = null; 
        Part filePart = request.getPart("file"); 
        MLogger.log("" + request.getParts());
        if(filePart == null){
            model.addAttribute("message", "File was null!"); 
            return "raw"; 
        }
        
        in = filePart.getInputStream();
        MemeSQL.addPhoto(u.getID(), in); 
        
        model.addAttribute("message", "File uploaded!"); 
        return "raw"; 
        
      }catch(Exception e){
          e.printStackTrace();
          MLogger.log("Something went wrong!"); 
      }
      
      
      
      
     return "raw"; 
   }
}