The way this works is makes a giant java bean. Web.xml tells it to make "infodingding" as a dispatcher, and then makes it so the servelet "infodingding" is mapped
to /. 

The <servlet><servlet-mapping>THING is what tells Java to look for a THING-servlet.xml to get more data about said servlet. 

The infodinding.xml tells it to look for any Spring MVC dispatchers under the com.tutorialspoint package. (Where HelloController lives!) 
and then uses this to make things go.


In theory, I think, you could create all your java files under here to do the heavy lifting and present information up. 